/**
 * The MIT License (MIT)
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */
import VerifyResult from './Result';
import { State } from '../containers/TicTacToe';

it('should announce winner as O (Row)', () => {
  const board = [
    [State.X, State.X, State.O],
    [State.O, State.O, State.O],
    [State.X, State.O, State.O]
  ];
  expect(VerifyResult(board)).toBe(State.O);
});

it('should announce winner as X (Column)', () => {
  const board = [
    [State.X, State.X, State.O],
    [State.X, State.O, State.O],
    [State.X, State.O, State.X]
  ];
  expect(VerifyResult(board)).toBe(State.X);
});

it('should announce winner as X (Diagonal)', () => {
  const board = [
    [State.X, State.X, State.O],
    [State.O, State.X, State.O],
    [State.X, State.O, State.X]
  ];
  expect(VerifyResult(board)).toBe(State.X);
});

it('should announce winner as Empty (Tie)', () => {
  const board = [
    [State.X, State.X, State.O],
    [State.O, State.O, State.X],
    [State.X, State.O, State.X]
  ];
  expect(VerifyResult(board)).toBe(State.Empty);
});