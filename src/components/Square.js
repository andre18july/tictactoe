import React from 'react';
import './Square.css';

const square = props => {

    console.log(props.value);

    let classDef;
    if(props.gameState !== 'playing' || props.value !== null){
        classDef = "Square-Disabled";
    }

    if(props.gameState === 'playing'){
        classDef = "Square";
    }

    if(props.victorySquare){
        classDef = classDef + " green";
    }

    return(

        <div className={classDef} onClick={ props.gameState === 'playing' ? props.click : null } >
            <p style={{fontSize: "3em"}}>{props.value}</p>
        </div>
    )

}


export default square;