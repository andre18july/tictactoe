import React, { Component } from 'react';
import Square from '../components/Square';
import VerifyResult from '../util/Result';
import './TicTacToe.css';

export const State = {
    Empty: null,
    X: 'X',
    O: 'O'
};    


class TicTacToe extends Component {


    constructor(props){
        super(props);
        this.state = {
            board: [
                [State.Empty,State.Empty,State.Empty], 
                [State.Empty,State.Empty,State.Empty], 
                [State.Empty,State.Empty,State.Empty]
            ],
            currentPlayer: Math.floor(Math.random() * 2) === 0 ? State.X : State.O,
            boardState: 'playing',
            victorySquaresPos: [false,false,false,false,false,false,false,false,false]
        }



    }

    setVictorySquares = (victorySquares) => {
        if(victorySquares !== null){
            let newVictorySquaresPos = [...this.state.victorySquaresPos];
            newVictorySquaresPos[victorySquares[0]] = true;
            newVictorySquaresPos[victorySquares[1]] = true;
            newVictorySquaresPos[victorySquares[2]] = true;
            this.setState({
                victorySquaresPos: newVictorySquaresPos
            })
        } 
    }


    handlerSquareClick = (line, col) => {
        
        if(this.state.board[line][col] !== State.Empty){
            return;
        }

        const newBoard = [...this.state.board];
        newBoard[line][col] = this.state.currentPlayer;

        const Winner = VerifyResult(newBoard) === null ? 'playing' :  VerifyResult(newBoard).result;
        if(Winner !== 'playing'){
            this.setVictorySquares(VerifyResult(newBoard).victorySquares);
        }
        
        this.setState(prev => { return {
            board: newBoard,
            currentPlayer: prev.currentPlayer === State.X ? State.O : State.X,
            boardState: Winner
        }})

    }

    restartGame = () => {
        this.setState({
            board: [
                [State.Empty,State.Empty,State.Empty], 
                [State.Empty,State.Empty,State.Empty], 
                [State.Empty,State.Empty,State.Empty]
            ],
            currentPlayer: Math.floor(Math.random() * 2) === 0 ? State.X : State.O,
            boardState: 'playing',
            victorySquaresPos: [false,false,false,false,false,false,false,false,false]
        })
    }



    render() {

        const boardState = this.state.boardState;

        let playState = null;


        if(boardState !== 'playing'){

            if(boardState === State.O){
                playState = <h2>Victory for player {State.O}</h2>;
            }else if(boardState === State.X){
                playState = <h2>Victory for player {State.X}</h2>;
            }else{
                playState = <h2>Game Draw</h2>;
            }
        }

        


        return (
            <div className="TicTacToe">
                <div className="Title">
                    <h1 className="Title-Master">TiC TaC ToE</h1>
                    <div>A simple game for complex minds...</div>
                </div>

                {playState}
                

                {boardState === 'playing' ? <p style={{height: '1.5em', margin: '25px 0'}}>Player Turn: {this.state.currentPlayer}</p> : null }

                {/*<div className={boardState !== 'playing' ? "Board pink" : "Board"}>*/}
                <div className="Board">
                    <div className="flex">
                        <Square click={boardState === 'playing' ? () => this.handlerSquareClick(0,0) : null} value={this.state.board[0][0]} victorySquare={this.state.victorySquaresPos[0]} gameState={this.state.boardState}/>
                        <Square click={boardState === 'playing' ? () => this.handlerSquareClick(0,1) : null} value={this.state.board[0][1]} victorySquare={this.state.victorySquaresPos[1]} gameState={this.state.boardState}/>
                        <Square click={boardState === 'playing' ? () => this.handlerSquareClick(0,2) : null} value={this.state.board[0][2]} victorySquare={this.state.victorySquaresPos[2]} gameState={this.state.boardState}/>
                    </div>

                    <div className="flex">
                        <Square click={boardState === 'playing' ? () => this.handlerSquareClick(1,0) : null} value={this.state.board[1][0]} victorySquare={this.state.victorySquaresPos[3]} gameState={this.state.boardState}/>
                        <Square click={boardState === 'playing' ? () => this.handlerSquareClick(1,1) : null} value={this.state.board[1][1]} victorySquare={this.state.victorySquaresPos[4]} gameState={this.state.boardState}/>
                        <Square click={boardState === 'playing' ? () => this.handlerSquareClick(1,2) : null} value={this.state.board[1][2]} victorySquare={this.state.victorySquaresPos[5]} gameState={this.state.boardState}/>
                    </div>

                    <div className="flex">
                        <Square click={boardState === 'playing' ? () => this.handlerSquareClick(2,0) : null} value={this.state.board[2][0]} victorySquare={this.state.victorySquaresPos[6]} gameState={this.state.boardState}/>
                        <Square click={boardState === 'playing' ? () => this.handlerSquareClick(2,1) : null} value={this.state.board[2][1]} victorySquare={this.state.victorySquaresPos[7]} gameState={this.state.boardState}/>
                        <Square click={boardState === 'playing' ? () => this.handlerSquareClick(2,2) : null} value={this.state.board[2][2]} victorySquare={this.state.victorySquaresPos[8]} gameState={this.state.boardState}/>
                    </div>
                </div>


                {boardState !== 'playing' ? <button onClick={this.restartGame}>Play Again!</button> : null }

            </div>
        );
    }



}

export default TicTacToe;