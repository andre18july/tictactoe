import React from 'react';
import TicTacToe from './containers/TicTacToe';
import './App.css';


function App() {
  return (
    <div className="App">
      <TicTacToe />
    </div>
  );
}

export default App;
